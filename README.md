# Start your project #

Hi, i made this to make this world a better place for everyone.

### What is this repository for? ###

* Everyone who likes to work fast and use modern techniques.
* Version 1

### How do I get set up? ###

* fork repo
* clone to workplace / or fork again to give it your own name
* bower install, npm install in commandline
* grunt watch
* start working!

### Contribution guidelines ###

* just send pull command i'll review and update

### How does is manage files ###

**files are converted like this:**

- jade > html > min html
- sass > css > min css
- js > min js

### Remarks ###

* grunt automaticly checks files, no more adding files
* watch out for making changes in css or html folder in dev since making changes in jade or sass folder will overwrite them