module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks("grunt-modernizr");
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.initConfig({

    watch: {
      jade: {
        files: ['dev/jade/*.jade'],
        tasks: ['html'],
        options: {
          livereload: true
        }
      },
      sass: {
        files: ['dev/sass/*.sass', 'dev/scss/*.scss'],
        tasks: ['css'],
        options: {
          livereload: true
        }
      },
      html: {
        files: ['dev/html/*.html'],
        options: {
          livereload: true
        }
      },
      js: {
        files: ['dev/js/*.js'],
        tasks: ['uglify'],
        options: {
          livereload: true
        }
      }
    },
    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: [{
          expand: true,
          cwd: 'dev/js',
          src: ['**/*.js'],
          dest: 'app/js/'
        }]
      }
    },

    sass: {
      target: {
        files: [{
            expand: true,
            cwd: 'dev/sass/',
            src: ['**/*.sass', '!**/_*.sass'],
            dest: 'dev/css/',
            expand: true,
            ext: ".css"
        }]
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 8', 'ie 9']
      },
      target: {
        src: 'dev/css/**/*.css'
      }
    },

    postcss: {
      options: {
        processors: [
          require('cssgrace')
        ]
      },
      target: {
        src: 'dev/css/**/*.css'
      }
    },

    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'dev/css/',
          src: ['**/*.css'],
          dest: 'app/css/',
          ext: ".min.css"
        }]
      }
    },

    htmlmin: {
      target: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: [{
          expand: true,
          cwd: 'dev/html',
          src: '**/*.html',
          dest: 'app/'
        }]
      }
    },

    jade: {
      compile: {
        options: {
            client: false,
            pretty: true
        },
        files: [{
          cwd: "dev/jade",
          src: "**/*.jade",
          dest: "dev/html",
          expand: true,
          ext: ".html"
        }]
      }
    },

    modernizr: {

      dist: {
        // [REQUIRED] Path to the build you're using for development.
        "devFile" : "bower_components/modernizr/modernizr.js",

        // Path to save out the built file.
        "outputFile" : "app/js/modernizr-custom.js",

        // Based on default settings on http://modernizr.com/download/
        "extra" : {
            "shiv" : true,
            "printshiv" : true,
            "load" : true,
            "mq" : false,
            "cssclasses" : true
      },

      // Based on default settings on http://modernizr.com/download/
      "extensibility" : {
          "addtest" : false,
          "prefixed" : false,
          "teststyles" : false,
          "testprops" : false,
          "testallprops" : false,
          "hasevents" : false,
          "prefixes" : false,
          "domprefixes" : false,
          "cssclassprefix": ""
      },

      // By default, source is uglified before saving
      "uglify" : true,

      // Define any tests you want to implicitly include.
      "tests" : [],

      // By default, this task will crawl your project for references to Modernizr tests.
      // Set to false to disable.
      "parseFiles" : true,

      // When parseFiles = true, this task will crawl all *.js, *.css, *.scss and *.sass files,
      // except files that are in node_modules/.
      // You can override this by defining a "files" array below.
      // "files" : {
          // "src": []
      // },

      // This handler will be passed an array of all the test names passed to the Modernizr API, and will run after the API call has returned
      // "handler": function (tests) {},

      // When parseFiles = true, matchCommunityTests = true will attempt to
      // match user-contributed tests.
      "matchCommunityTests" : false,

      // Have custom Modernizr tests? Add paths to their location here.
      "customTests" : []
    }
  }

  });
  grunt.registerTask('css', ['sass', 'autoprefixer', 'postcss', 'cssmin']);
  grunt.registerTask('html', ['jade', 'htmlmin']);
  grunt.registerTask('build', ['css', 'html', 'modernizr']);
}