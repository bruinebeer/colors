//-----------------------------------------------------
//
// FUNCTIONS
//
//-----------------------------------------------------

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

//-----------------------------------------------------
// Close Menu Overlay
//-----------------------------------------------------

function closeMenu(){
	$(".overlay-nav").hide();
}

//-----------------------------------------------------
// Overlay GRID
//-----------------------------------------------------

function generateGrid () {

	//loop that renders divs
	for (var i = 0; i < 200; i++) {

		//add divs to overlay
		$(".grid-overlay").append('<div class="overlay-row hidden"></div>');
	}

	//toggle grid btn handler & function
	$(".overlay-btn").on("click", function() {
		$(".grid-overlay").toggle();
	})
}

//-----------------------------------------------------
// Detect browser (for scrolling issues)
//-----------------------------------------------------

function checkBrowser (browser, multiplyer) {
	if (browser) {
		scrollSpeed = scrollSpeed * multiplyer;
	}
}
// Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

// Firefox 1.0+
var isFirefox = typeof InstallTrigger !== 'undefined';

// At least Safari 3+: "[object HTMLElementConstructor]"
var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

// Chrome 1+
var isChrome = !!window.chrome && !isOpera;

// At least IE6
var isIE = /*@cc_on!@*/false || !!document.documentMode;

// speedcontol happens here
checkBrowser(isFirefox, .3);
checkBrowser(isChrome, 3);
checkBrowser(isIE, 1.5);

if(isIE){
	// put ie specific code here
}
if(isFirefox){
	//put firefox specific code here
}


// <- FUNCTION END
//-----------------------------------------------------

//-----------------------------------------------------
//
// VARIABLES
//
//-----------------------------------------------------
var randomNumber

$(document).ready(function(){
	randomNumber = Math.random();
});


// <- VARS END
//-----------------------------------------------------

//-----------------------------------------------------
//
// CODE EXECUTION
//
//-----------------------------------------------------

//start up grid
generateGrid();



